extends Node2D

var WAIT_TIME = 4
var timer = 0.1
var count = 100
var generation_area = []

var brick = load("res://tiles/brick_normal.tscn")

var brick_size
var brick_slot

func _enter_tree():
	if !has_node("field"):$backup_field.name = "field"
	else:$backup_field.queue_free()
	generation_area = [$field.rect_global_position,$field.rect_size]
	
	
	var reference = brick.instance()
	
	for i in reference.get_children():
		print(i.name)
	if !reference.has_node("size"):queue_free()
	brick_size = reference.get_node("size").rect_size+Vector2(5,5)
	reference.queue_free()
	brick_slot = generation_area[1]/brick_size
	brick_slot = [int(brick_slot.x-1),int(brick_slot.y-1)]
	
#	size = [int(size.x),int(size.y)]
#	reference.global_position = generation_area[0]
#	reference.name = "refrence"
#	get_parent().add_child(reference)




func _process(delta):
	timer -=delta
	if timer >0:
		return
	timer = WAIT_TIME
	WAIT_TIME = max(0.2,WAIT_TIME-0.3)
	#pick a slot
	var slot = Vector2(randi()%brick_slot[0]+1,randi()%brick_slot[1]+1)
#	print("hslot: ",randi()%brick_slot[0]+1, " of: ", brick_slot[0]+1)
#	print("vslot: ",randi()%brick_slot[1]+1, " of: ", brick_slot[1]+1)
	
#	var pos = Vector2(randi()%int(generation_area[1].x),randi()%int(generation_area[1].y))
	var pos = slot*brick_size+generation_area[0]
#	pos += generation_area[0]
	var placebrick = brick.instance()
	placebrick.global_position = pos
	get_parent().add_child(placebrick)
	count -=1
	if count <=0:
		print("placed enough bricks. Stop here")
		queue_free()
	else:print("remianing: ",count)
	
