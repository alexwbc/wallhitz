extends RigidBody2D

var direction = Vector2(0.0,1.0)

var vel = Vector2()
var vel_old = Vector2()
#var direction_old = Vector2()#control when ball change direction
var speed = 600
#var vel = Vector2()
var state = "move_fast"

var anim = "move_slow"
var anim_new = "move_fast"
var slowtime = null


var reflect = null
var plug_reflect = [null,null]
var plug_bounce = null

var util_scripts = {"scale2nothing":load("res://actor/player/ball_tools/scale2nothing.gd")}

func _enter_tree():
	set_max_contacts_reported(1)
	connect("body_entered",self,"hit_something")

func hit_something(what):
	anim_new = "move_slow"
	slowtime = 0.2
	var groups = what.get_groups()
	if groups.has("player"):
#		what.ballhit(global_position.direction_to(what.global_position))
		what.ballhit(atan2(what.global_position.y-global_position.y,what.global_position.x-global_position.x))


#atan2(targetY-gunY, targetX-gunX)
		var relative_impact = (global_position.x-what.global_position.x)/what.paddle_size#+(what.paddle_size*0.5))/what.paddle_size
		if plug_reflect == [null,null]:
			plug_reflect = [Vector2(),what]
		plug_reflect[0].x =relative_impact
	if groups.has("scene"):reflect = what
	if groups.has("brick"):
		
#		var dir = global_position.direction_to(what.global_position)
		var dir = atan2(what.global_position.y-global_position.y,what.global_position.x-global_position.x)
#		what.ballhit(dir)
		what.ballhit(direction)
		reflect = what

func base_reflection(state,on_what):
	var bodies = get_colliding_bodies()
	var base_dir = Vector2()
	for b in range(bodies.size()):
		if bodies[0] == on_what:
			var bdir = state.get_contact_local_normal(b)
			if bdir.x <= -0.8  or 0.8 <= bdir.x:
				base_dir.x = -direction.x
			elif bdir.y <= -0.8  or 0.8 <= bdir.y:
				base_dir.y = -direction.y
			else:
				base_dir =bdir.normalized()
			if abs(base_dir.y)<0.1:
				if base_dir.y >0:base_dir.y=0.3
				else:base_dir.y=-0.3

	return base_dir.normalized()
#					direction = direction.normalized()
#
#				extra_power += 200


func _process(delta):
	var vel = linear_velocity.normalized()
	
#	if anim != anim_new:
#		$anim.play(anim_new)
#		anim = anim_new

	if slowtime != null:
		slowtime -=delta
		if slowtime<=0:
			slowtime = null
			anim_new = "move_fast"

#	if direction != direction_old:
#		$anim.play("accellerate")
#		print("change direction")
#		direction_old = direction

	if vel != vel_old:

		$anim.play("accellerate")
		$anim.seek(0,true)

		var residual_sprite = $fx/bounce.duplicate()
#		residual_sprite.set_script(util_scripts["scale2nothing"])
		get_parent().add_child(residual_sprite)
		residual_sprite.set_owner(get_parent())
		residual_sprite.global_position = global_position
		residual_sprite.playing = true
		residual_sprite.visible = true
		residual_sprite.connect("animation_finished",residual_sprite,"queue_free")
#		residual_sprite.look_at((vel_old*10)+global_position)
		residual_sprite.look_at(linear_velocity)
		$sprite.look_at(direction+global_position)

		vel_old = vel


func _integrate_forces(state):
	angular_velocity =0
	var cur_dir = linear_velocity.normalized()
	var extra_power = 0
	if reflect != null:
		var bodies = get_colliding_bodies()
		for b in range(bodies.size()):
			if bodies[0] == reflect:
				var bdir = state.get_contact_local_normal(b)
				if bdir.x <= -0.8  or 0.8 <= bdir.x:
					direction.x = -direction.x
				elif bdir.y <= -0.8  or 0.8 <= bdir.y:
					direction.y = -direction.y
				else:
					direction =bdir.normalized()
				if abs(direction.y)<0.1:
					if direction.y >0:direction.y=0.3
					else:direction.y=-0.3
					direction = direction.normalized()

				extra_power += 200
		reflect = null
	if plug_reflect != [null,null]:
		if plug_reflect[0] != null:
			direction = base_reflection(state,plug_reflect[1])
		direction += plug_reflect[0]
		direction.normalized()
		plug_reflect = [null,null]
	
#	linear_velocity = direction*(speed+extra_power)
	if plug_bounce != null:
		
		linear_velocity = plug_bounce#+(-linear_velocity.normalized())*10
		direction = linear_velocity.normalized()
		plug_bounce = null
	else:
		linear_velocity = direction*(speed+extra_power)
	rotation = 0
