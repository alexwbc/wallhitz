extends Node

var slowtime_active = false
var slowtime
const SCALE_SPD = 5

var timescale = 1
const LIMIT = 0.3
func _ready():
	set_process(false)



func _process(delta):

	if !Input.is_action_pressed("slowtime"):
		slowtime_active = false


	if slowtime_active:
		timescale = max(timescale-1*delta*SCALE_SPD,LIMIT)
		Engine.time_scale = timescale
	else:
		timescale+=1*delta*SCALE_SPD
		if timescale>=1:
			timescale = 1
			Engine.time_scale = 1
			set_process(false)
			return
		Engine.time_scale = timescale



func activate_slowtime():
	if slowtime_active:return
	timescale = 1
	slowtime_active = true
	set_process(true)
