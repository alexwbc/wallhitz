extends Node


var running = false


var fx_db = ["shake"]
var current_fx
var paddle



const shake_amount = 10
var shake


func _enter_tree():
	paddle = get_parent().get_parent()

func pro_shake(delta):
	shake -= 20*delta
	var pos = int(shake)
	if pos <=0:
		set_process(false)
		running = false
		return
	pos = randi()%pos-(pos*0.5)
	var ranshake = Vector2(pos,pos)
	paddle.get_node("sprite").position = ranshake


func _ready():
	set_process(false)

func start(which, from):
	if !is_processing():
		set_process(true)
	running = true
	if which == "shake":
		current_fx = "pro_shake"
		shake = shake_amount

func _process(delta):
	if running:
		call(current_fx,delta)
