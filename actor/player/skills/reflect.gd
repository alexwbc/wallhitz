extends Node2D

var ball

const DIST_MAX = 100
var pos = Vector2()
var dist = 0
var rot

var exit = false

func _enter_tree():
	ball = get_parent()
	update_cur(pos)



func update_cur(relative):
	if pos.x == 0:
		pos.x = ball.linear_velocity.x
	if pos.y == 0:
		pos.y = ball.linear_velocity.y
	pos= (pos+relative).clamped(DIST_MAX)
	dist = pos.length()/DIST_MAX
	rot = pos.angle()+1.5
	$icon.scale = Vector2(dist,dist)
	$icon.position = pos
	$icon.rotation = rot
func _input(ev):
	if ev.get_class() == "InputEventMouseMotion":
		update_cur(ev.relative)

#		pos= (pos+ev.relative).clamped(DIST_MAX)
#		dist = pos.length()/DIST_MAX
#		rot = pos.angle()+1.5
#		$icon.scale = Vector2(dist,dist)
#		$icon.position = pos
#		$icon.rotation = rot



func _process(delta):
	if !exit and !Input.is_action_pressed("alt_action"):
		exit = true
		ball.plug_reflect == [null,rot]
		
		ball.plug_bounce = pos.normalized()*(dist*300)
		

	if exit:
		modulate.a -= 10*delta
		if modulate.a <=0:
			queue_free()
