extends StaticBody2D


var level
var rail = null
var rail_car = PathFollow2D.new()
var rail_limit = Vector2()
var dir_h = 1

var paddle_size_db = {}
var paddle_size
const SPEED_MAX = 10

var state = "normal"
var state_new = "normal"

var bounce_ball = null

var skill_reflect = preload("res://actor/player/skills/reflect.tscn")

func _enter_tree():
	paddle_size_db["normal"] = $ref_size_normal.rect_size.x
	$ref_size_normal.queue_free()
	paddle_size = paddle_size_db["normal"]
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	level = get_parent()
	if !level.has_node("player_rail"):queue_free()
	rail = level.get_node("player_rail")
	rail_car.loop = false
	rail.add_child(rail_car)
	rail_car.unit_offset = 0.5




func _process(delta):

	if Input.is_action_pressed("paddle_left"):dir_h = -SPEED_MAX
	elif Input.is_action_pressed("paddle_right"):dir_h = SPEED_MAX

	if bounce_ball !=null:#timer for when a ball bounce on paddle (used for fx animation control)
		bounce_ball -=1*delta
		pass






func _input(event):
	if Input.is_action_just_pressed("alt_action"):
		$logics/slowtime.activate_slowtime()
		var skill = skill_reflect.instance()
		get_parent().get_node("ball").add_child(skill)


func _physics_process(delta):
	match state:
		"normal":phy_normal(delta)
	if state_new != state:switch_state()


func ballhit(from):
	$logics/fx.start("shake", from)


func phy_normal(delta):
	if dir_h != null:
		rail_car.offset += dir_h
		global_position = rail_car.global_position
	
	
	dir_h = null

func switch_state():
	state = state_new
