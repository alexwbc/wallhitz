extends Sprite

#this script, attached to a sprite, resize the sprite then queue_free it
#used for: fire/flame/fx animation when the ball bounce on wall (the residual flame is scaled down)

func _process(delta):
	var sca = scale.x-10*delta
	scale = Vector2(sca,sca)
	if scale.x <=0:queue_free()
