extends Node2D

func _enter_tree():
	base.main = self

func _input(event):
	if Input.is_action_just_pressed("ui_accept"):
		
		shoot(get_viewport().get_mouse_position())
func shoot(pos):
#	print("operating")
	var sprite = Sprite.new()
	sprite.texture = load("res://icon.png")
	sprite.scale = Vector2(0.2,0.2)
	sprite.global_position = pos
	sprite.modulate = Color("ff0000")
	var timer = Timer.new()
	timer.autostart = true
	sprite.add_child(timer)
	add_child(sprite)
	timer.connect("timeout",sprite,"queue_free")
	
#	print(pos)
