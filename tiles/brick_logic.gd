extends StaticBody2D

var broken = false
var break_dir = Vector2()
func _ready():
	
	set_process(false)

func _process(delta):
	if broken:
		if !$body.disabled:
			$body.disabled = true
		$shape.global_position+=break_dir*delta*100
		$shape.modulate.a -= 1*delta
		if $shape.modulate.a <= 0:queue_free()

func ballhit(direction):
	broken = true
	break_dir = direction
	set_process(true)
